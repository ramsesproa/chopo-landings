var gulp        = require('gulp');
var minimist    = require('minimist');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var rename      = require("gulp-rename");


// Static Server + watching scss/html files
gulp.task('browser-sync', function() {
    browserSync.init({
    	startPath: "pages/",
        server: {
        	//baseDir: "pages/"
        }
    });

    gulp.watch("**/assets/scss/*.scss", gulp.series('sass'));
    gulp.watch("pages/**/*.html").on('change', browserSync.reload);
});


// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
	console.log(minimist);
    return gulp.src("pages/**/assets/scss/*.scss")
        .pipe(sass({
        	outputStyle: 'compressed'
        }))
        // *.pipe(gulp.dest("dist/pages/"))
        .pipe(rename(function (path) {
        	path.dirname += "/../css"; }
        ))
        .pipe(gulp.dest('./pages/'))
        .pipe(browserSync.stream());
});

gulp.task('default', gulp.series('browser-sync','sass'));