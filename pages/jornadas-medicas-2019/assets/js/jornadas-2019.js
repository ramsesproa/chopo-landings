$(document).ready(function() {

	$(".agenda-nav-link").on('click', function(evt){
		evt.preventDefault();
		$(".agenda-nav-item").removeClass('active');
		idState = $(this).attr('href');
		idState = idState.replace("#","");
		
		getAgenda(idState);
		
		$(this).parent().addClass('active');
	});

	function getAgenda(id){
		$.ajax({
			url: 'https://jornadas-medicas.s3.amazonaws.com/jornadas-2019.json',
			//url: 'assets/js/jornadas-2019.json',
			dataType: 'json',
  			contentType: 'json',
			beforeSend: function(){
		    	// Show image container
		   		$(".loading").show();
		   	},
			success: function(respuesta) {

				//$(".loading").slideDown('slow');
				html = "";
				
				
				//console.log(respuesta);
				//console.log(respuesta[id]);
				var stateFull = respuesta[id];
				//var stateTag = stateFull.stateName;
				var stateLocation = stateFull.location;
				var stateAgenda = stateFull.agenda;
				//console.log(stateAgenda);
				
				var modalCounter = 0;

				for (var i = 0; i < stateAgenda.length; i++) {

					if (stateAgenda.length == 1) {
						
						html += '<div class="col-md-6 offset-md-3">';
					} else {
						html += '<div class="col-md-6">';
					}
					
					//html += '<div class="col-md-6">';
					html += '<section class="agenda-day"><h3 class="agenda-day-name">'+stateAgenda[i]["dayName"]+'</h3><h2 class="agenda-day-date">'+stateAgenda[i]["dayDate"]+'</h2>';
					
					var agendaContent = stateAgenda[i]["content"];
					
					for (var a = 0; a < agendaContent.length; a++) {						
						
						var content = agendaContent[a];
						
						html += '<article class="agenda-item">';
						if (content.hour){
							html += '<p class="agenda-time"><time class="agenda-time">'+content.hour+'</time></p>';
						}
						html += '<div class="agenda-item-content">';
						html += '<h2 class="agenda-item-title">'+content.title+'</h2>';

						var html_title,
							html_modal = "";

						if(content.doctors){
						//if(content.speakerName&&content.speakerCV){

							
							var speakerTitle = "";
							//var speakerLink = "";
							var speakerModal = "";

							for (var dr = 0; dr < content.doctors.length; dr++) {

								var speakerName = content.doctors[dr].speakerName;
								//console.log(speakerName);

								//SPEAKER TITLE
								//speakerTitle += '';
								
								var speakerCV ="";
								// CV LIST
								for (var cv = 0; cv < content.doctors[dr].speakerCV.length; cv++) {
									speakerCV += '<li>'+content.doctors[dr].speakerCV[cv]+'</li>';
								}
								// MODAL CV DOCTOR
								speakerModal += '<div class="modal fade agenda-modal-dr-'+modalCounter+'"  tabindex="-1" role="dialog" aria-labelledby="agenda-modal-dr-'+modalCounter+'" aria-hidden="true">'
									speakerModal += '<div class="modal-dialog modal-dialog-centered">';
										speakerModal += '<div class="modal-content">';
											speakerModal += '<div class="modal-header"><h5 class="modal-title">CV compacto</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
											speakerModal += '<div class="modal-body"><p>'+speakerName+'</p><ul>'+speakerCV+'</ul></div>';
										speakerModal += '</div>';
									speakerModal += '</div>';
								speakerModal += '</div>';

								//console.log(content.doctors[dr].speakerName);

								speakerTitle += '<a href="#" data-toggle="modal" data-target=".agenda-modal-dr-'+modalCounter+'" class="agenda-speaker"><span class="speaker-name"><span class="icon icon-uE006-checkups2"></span>'+speakerName+'</span></a>';
								

								modalCounter++;

							}

							//console.log(speakerTitle);
							//console.log(speakerModal);
							if (content.doctors.length>1){var ponente = "s"}else{var ponente = ""}
							
							html += '<span class="speaker-title">Ponente'+ponente+'</span>';
							//html += '<a href="#" data-toggle="modal" data-target=".agenda-modal-dr-'+modalCounter+'" class="agenda-speaker">'+speakerTitle+'</a>';
							html += speakerTitle+speakerModal;

						}
						
						html += '</div>';
						html += '</article>';

					}
					
					html += '</section></div>';
					
				}

				if (stateLocation){

					var venue = stateLocation.venue;
					var address = stateLocation.address;
					var especialities = stateLocation.especialities;
					var esp_html = "";
					for (var i = 0; i < especialities.length; i++) {
						esp_html += "<li>"+especialities[i]+"</li>";
					}

					var location = $(".location");

					location.find(".location-venue").find("ul").html(esp_html);
					location.find(".location-venue").find("h4").text(venue);
					location.find(".location-venue").find("p").text(address);

					//Phone
					var phone = $(".ft-phone");
					var phoneHash = phone.attr("href");
					var phoneString = stateLocation.phone_string;
					var phoneCall = stateLocation.phone_call;

					phone.text(phoneString);
					phone.attr('href', 'tel:'+phoneCall);

				} else {
					console.log("No hay location para "+id);
				}


				//$(".loading").addClass('class_name')('slow');
				$(".row-agenda").html(html);
				$(".location").show();
				
				/*stateAgenda.each(function(index, item) {
					console.log(item);
				});*/


				//console.log(stateFull);
				//console.log(stateTag);
				//console.log(stateAgenda);

//				var agendaTemplate = $(".agenda-day").html();
//				console.log(agendaTemplate);

			},
			complete:function(data){
    			// Hide image container
    			$(".loading").hide();
  			},
			error: function() {
		        console.log("No se ha podido obtener la información");
		    }
		});
	}
	
});